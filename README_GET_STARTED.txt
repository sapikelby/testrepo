This is a test repo I created to check whether everyone was able to create a BitBucket account.

Make sure you are able to push and pull from this repository once you do get everything setup on your computer. 

I'm using Git to push and pull. I mainly use that because I have a Github account so I got used to using that commandline tool.

Check out the GitGuide if you've never used Git or other commandline tools. That guide, published by a friend of mine, is for GitHub, but it's essentially the same

for Bitbucket. 


Download Git commandline tool
http://www.git-scm.com/

Tutorial for Git
https://try.github.io/levels/1/challenges/1


Setup SSH for Git (Same instructions as found in the GitGuide)
https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git

// comment for uploading